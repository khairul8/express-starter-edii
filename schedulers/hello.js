const CronJob = require("node-cron");

exports.hello = () => {
  const job = CronJob.schedule("*/1 * * * *", () => {
    console.log("Scheduler is running");
  });

  job.start();
}
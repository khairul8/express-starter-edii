const {db} = require('./vars')

module.exports={
	host: db.host,
	user: db.user,
	password: db.pass,
	db: db.name,
	port: db.port,
	dialect: "mysql"
};
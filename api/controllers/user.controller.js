const httpStatus = require('http-status');
const logger = require('../config/logger');
const db = require('../models')
const Users = db.user
const Profiles = db.profiles
const Histories = db.history

exports.list = async (req, res) => {
  try {
    const users = await Users.findAll({include: [Profiles, Histories]})
    res.status(httpStatus.OK)
    return res.json({status: "OK" , users})
  } catch (error) {
    logger.error('error', error)
    res.status(httpStatus.INTERNAL_SERVER_ERROR)
    return res.json({status: "ERR", message: "Internal Server Error"})
  }
};

exports.detail = async (req, res) => {
  try {
    const users = await Users.findOne({
      where: {
        id: req.params.id
      },
      include: [
        Profiles, Histories
      ]
    })
    res.status(httpStatus.OK)
    return res.json({status: "OK", users})
  } catch (error) {
    logger.error('error', error)
    res.status(httpStatus.INTERNAL_SERVER_ERROR)
    return res.json({status: "ERR", message: "Internal Server Error"})
  }
};

exports.store = async (req, res) => {
  let transaction;
  try {
    const bodyProfile = req.body.profiles
    delete req.body.profile
    const bodyUser = req.body
    transaction = await db.sequelize.transaction()
    const storeUsers = await Users.create(bodyUser, {transaction})
    bodyProfile.user_id = storeUsers.id
    await Profiles.create(bodyProfile, {transaction})
    await transaction.commit()
    res.status(httpStatus.CREATED)
    return res.json({status: "CREATED"})
  } catch (error) {
    logger.error('error', error)
    await transaction.rollback()
    res.status(httpStatus.INTERNAL_SERVER_ERROR)
    return res.json({status: "ERR", message: "Internal Server Error"})
  }
};

exports.update = async (req, res) => {
  try {
    const body = req.body
    await Users.update(body, { where: { id: req.params.id } })
    res.status(httpStatus.OK)
    return res.json({status: "OK"})
  } catch (error) {
    logger.error('error', error)
    res.status(httpStatus.INTERNAL_SERVER_ERROR)
    return res.json({status: "ERR", message: "Internal Server Error"})
  }
};
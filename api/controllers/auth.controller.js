const httpStatus = require('http-status');
const logger = require('../config/logger');
const Helper = require('../utils/helper');
const _ = require('lodash')
const db = require('../models');
const Users = db.user
const AccessToken = db.access_token
const Histories = db.history

exports.login = async (req, res) => {
  try {
    const cekUser = await Users.findOne({
      where: {
        username: req.body.username,
        password: req.body.password
      }
    })
    if (_.isNil(cekUser)) {
      res.status(httpStatus.NOT_FOUND)
      return res.json({status: "ERR", message: "User not found"})
    }

    const cekToken = await AccessToken.findOne({
      where: {
        user_id: cekUser.id
      }
    })

    const body = {}
    const token = Helper.generate_token()
    const expire_in = Math.ceil((new Date().getTime() + 1000 * 60 * 15) / 1000)
    body.access_token = token
    body.expire_in = expire_in;
    body.user_id = cekUser.id

    if (cekToken) {
      await AccessToken.update(body, {
        where: {
          id: cekToken.id
        }
      })
    } else {
      await AccessToken.create(body);
    }

    await Histories.create({
      flag: 'login',
      user_id: cekUser.id
    })

    const tokens = {
      access_token: token,
      expire_in
    }

    res.status(httpStatus.OK)
    return res.json({status: "OK", data: tokens})
  } catch (error) {
    logger.error('error', error)
    res.status(httpStatus.INTERNAL_SERVER_ERROR)
    return res.json({status: "ERR", message: "Internal Server Error"})
  }
};

exports.check = async (req, res) => {
  try {
    res.status(httpStatus.OK)
    return res.json({status: "OK", data: req.user_session})
  } catch (error) {
    logger.error('error', error)
    res.status(httpStatus.INTERNAL_SERVER_ERROR)
    return res.json({status: "ERR", message: "Internal Server Error"})
  }
};

exports.logout = async(req, res) => {
  try {
    await Histories.create({
      flag: 'logout',
      user_id: req.user_session.user_id
    })

    res.status(httpStatus.OK)
    return res.json({status: "OK", message: "Logout success"})
  } catch (error) {
    logger.error('error', error)
    res.status(httpStatus.INTERNAL_SERVER_ERROR)
    return res.json({status: "ERR", message: "Internal Server Error"})
  }
}
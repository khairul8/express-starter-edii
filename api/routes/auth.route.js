const express = require('express');
const controller = require('../controllers/auth.controller');
const { authorize } = require('../middlewares/auth');

const router = express.Router();

router.route('/')
  .post(controller.login)
  .get(authorize, controller.check)

module.exports = router;

var DataTypes = require("sequelize").DataTypes;
var _access_token = require("./access_token");

function initModels(sequelize) {
  var access_token = _access_token(sequelize, DataTypes);

  access_token.belongsTo(users, { as: "user", foreignKey: "user_id"});
  users.hasMany(access_token, { as: "access_tokens", foreignKey: "user_id"});

  return {
    access_token,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;

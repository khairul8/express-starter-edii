const dbConfig = require("../config/database.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.db, dbConfig.user, dbConfig.password, {
  host: dbConfig.host,
  dialect: dbConfig.dialect,
  operatorsAliases: 0,
  logging: 0
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./users")(sequelize, Sequelize);
db.profiles = require("./profiles")(sequelize, Sequelize);
db.history = require("./history")(sequelize, Sequelize);
db.access_token = require("./access_token")(sequelize, Sequelize);

db.user.belongsTo(db.profiles, {foreignKey: 'id', targetKey: 'user_id'});
db.user.hasMany(db.history, {foreignKey: 'user_id', targetKey: 'id'});
db.user.belongsTo(db.access_token, {foreignKey: 'id', targetKey: 'user_id'});
db.access_token.belongsTo(db.user, {foreignKey: 'user_id', targetKey: 'id'})
// db.history.belongsTo(db.user, {foreignKey: 'user_id', targetKey: 'id'});

module.exports = db;
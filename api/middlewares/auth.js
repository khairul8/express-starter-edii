const httpStatus = require('http-status');
const _ = require('lodash')
const db = require('../models');
const Users = db.user
const AccessToken = db.access_token
const Histories = db.history
const Profiles = db.profiles


exports.authorize = async (req, res, next) => {
  const headers = req.headers.authorization
  if (!headers) {
    res.status(httpStatus.UNAUTHORIZED)
    return res.json({status: "ERR", message: "Bearer is required"})
  }

  const tokens = headers.replace('Bearer ', '')

  const users = await AccessToken.findOne({
    where: {
      access_token: tokens
    },
    include: [
      {
        model: Users,
        include: [Profiles, Histories]
      }
    ]
  })

  if (_.isNil(users)) {
    res.status(httpStatus.UNAUTHORIZED)
    return res.json({status: "ERR", message: "Token not found"})
  }

  if (new Date().getTime() > users.expire_in * 1000) {
    res.status(httpStatus.UNAUTHORIZED)
    return res.json({status: "ERR", message: "Token is expired"})
  }

  req.user_session = users
  return next();
};

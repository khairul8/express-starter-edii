const express = require('express');
const logger = require('./api/config/logger');
const {port, env} = require('./api/config/vars');
const routes = require('./api/routes');
const bodyParser = require('body-parser');
const db = require('./api/models');
const schedulers = require('./schedulers/hello');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', routes);

db.sequelize.sync()
.then(() => {
  logger.info("Connected to database.");
})
.catch((err) => {
  logger.error("Database connection failed: " + err.message);
});

app.listen(3000, () => logger.info(`server started on port ${port} (${env})`));

schedulers.hello()

module.exports = app;